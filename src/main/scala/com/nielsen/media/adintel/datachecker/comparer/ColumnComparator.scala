package com.nielsen.media.adintel.datachecker.comparer

import com.nielsen.media.adintel.datachecker.comparer.auwatch.AuWatchComparator
import com.nielsen.media.adintel.datachecker.comparer.smi.SMIComparator
import com.nielsen.media.adintel.datachecker.comparer.std.STDComparator
import com.nielsen.media.adintel.datachecker.comparer.wallmart.{Parser, WallMartComparator}
import com.nielsen.media.adintel.datachecker.{AppContext, CalculationResult}
import org.apache.spark.sql.SparkSession

trait ColumnComparator {

  def resolvedColumns: Seq[String]

  def compareColumns(): CalculationResult
}

object ColumnComparator {

  def resolve(appContext: AppContext)(implicit session: SparkSession): ColumnComparator = {
    appContext.job.toLowerCase.trim match {
      case _Comparators.Auwatch => AuWatchComparator(appContext)

      case _Comparators.WallMart => WallMartComparator(appContext, Parser.originalSubstring)

      case _Comparators.WallMartCSV => WallMartComparator(appContext, Parser.originalCsv)

      case _Comparators.SMI => SMIComparator(appContext)

      case _Comparators.STD => STDComparator(appContext)
    }
  }

  private[comparer] object _Comparators {
    private[comparer] val Auwatch = "auwatch"
    private[comparer] val WallMart = "wallmart"
    private[comparer] val WallMartCSV = "wallmartcsv"
    private[comparer] val SMI = "smi"
    private[comparer] val STD = "std"
  }

}
