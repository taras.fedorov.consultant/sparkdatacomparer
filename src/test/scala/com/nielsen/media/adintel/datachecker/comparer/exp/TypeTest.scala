package com.nielsen.media.adintel.datachecker.comparer.exp

import java.net.{ServerSocket, Socket}
import java.util.concurrent.{ExecutorService, Executors}

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TypeTest extends {


  @Test
  def run(): Unit = {
    new Thread(new NetworkService(2021, 2)).start()
    new NetworkService(2020, 2).run()
    assertEquals("expected", "result")
  }

  class NetworkService(port: Int, poolSize: Int) extends Runnable {
    val serverSocket = new ServerSocket(port)
    val pool: ExecutorService = Executors.newFixedThreadPool(poolSize)

    def run() {
      try {
        while (true) {
          // This will block until a connection comes in.
          val socket = serverSocket.accept()
          pool.execute(new Handler(socket))
        }
      }
      finally {
        pool.shutdown()
      }
    }
  }

  class Handler(socket: Socket) extends Runnable {
    println("Handler is created")

    def message: Array[Byte] = (Thread.currentThread.getName() + "\n").getBytes

    def run() {
      println(s"message = ${(message.map(_.toChar)).mkString}")
      Thread.sleep(2000)
      socket.getOutputStream.write(message)
      socket.getOutputStream.close()
    }
  }

}
