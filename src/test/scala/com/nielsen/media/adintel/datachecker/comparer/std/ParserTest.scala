package com.nielsen.media.adintel.datachecker.comparer.std


import com.nielsen.media.adintel.datachecker.comparer.RowInfo
import org.apache.spark.sql.SparkSession
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

object ParserTest {

  /*
  @Test
  def originalParseRowInfo(): Unit = {
    val input = "WABC (ABC) 15:56 7.19 2:13 5.37 7.19 2:13 5.37 7.19 2:13 5.37 6:41 5:26 1:15 0:00 0:45 8:28 7:26 0:06 0:54 7:27 90 10"

    val parseTransformF = Parser.originalParse _ andThen (_.toMFRowInfo)
    val actual: STDRowInfo = parseTransformF(input)

    assertEquals("WABC (ABC)", actual.NET_SH_NETWORK)
    assertEquals("15:56", actual.TOT_NPG)
    assertEquals("7.19", actual.POD_CNT)
    assertEquals("2:13", actual.LEN_POD)
    assertEquals("5.37", actual.UNIT_POD)
    assertEquals("7.19", actual.POD_CNT2)
    assertEquals("2:13", actual.LEN_POD2)
    assertEquals("5.37", actual.UNIT_POD2)
    assertEquals("7.19", actual.POD_CNT3)
    assertEquals("2:13", actual.LEN_POD3)
    assertEquals("5.37", actual.UNIT_POD3)
    assertEquals("6:41", actual.NAT_CML)
    assertEquals("5:26", actual.NET_CML)
    assertEquals("5:26", actual.BAR_CML)
    assertEquals("1:15", actual.NAT_PSA)
    assertEquals("0:00", actual.NAT_PRO)
    assertEquals("0:45", actual.LOC_NPG)
    assertEquals("8:28", actual.LOC_CML)
    assertEquals("7:26", actual.LOC_PSA)
    assertEquals("0:54", actual.LOC_PRO)
    assertEquals("7:27", actual.NAT_CMLPRO)
    assertEquals("90", actual.NAT_CMLPCT)
    assertEquals("10", actual.NAT_PROPCT)
    assertEquals(input, actual.text)
    assertEquals(RowInfo.MAINFRAME_SOURCE, actual.originalSource)
  }
*/

  @Test
  def originalParse1(): Unit = {
    val session: SparkSession = SparkSession.builder.master("local").getOrCreate()
    import session.sqlContext.implicits._

    val parsedDS = session.read.option("header", value = true).csv("src/test/resources/STD_sample.csv").as[STDRow]
    val actual: STDRowInfo = parsedDS.map(_.toMFRowInfo).head()

    assertEquals("ADSM", actual.NET_SH_NETWORK)
    assertEquals("14:32", actual.TOT_NPG)
    assertEquals(" 18", actual.NAT_PROPCT)
    val expectedText = "ADSM      ,14:32,06.07,02:23, 6.72,05.92,02:27, 6.89,05.90,02:27, 6.91,10:20,10:20,00:00,00:00,02:12,01:59,01:59,00:00,00:00,12:33, 82, 18"
    assertEquals(expectedText, actual.text)
    assertEquals(RowInfo.MAINFRAME_SOURCE, actual.originalSource)
  }

  @Test
  def originalParse2(): Unit = {
    val session: SparkSession = SparkSession.builder.master("local").getOrCreate()
    import session.sqlContext.implicits._

    val parsedDS = session.read.option("header", value = true).csv("src/test/resources/STD_sample.csv").as[STDRow]
    val actual: STDRowInfo = parsedDS.map(_.toMFRowInfo).head(2).last

    assertEquals("WABC", actual.NET_SH_NETWORK)
    assertEquals("15:52", actual.TOT_NPG)
    assertEquals(" 11", actual.NAT_PROPCT)
    val expectedText = "WABC (ABC),15:52,07.12,02:13, 5.98,07.12,02:13, 5.98,07.12,02:13, 5.98,06:35,05:23,01:11,00:01,00:46,08:29,07:27,00:09,00:52,07:21, 89, 11"
    assertEquals(expectedText, actual.text)
    assertEquals(RowInfo.MAINFRAME_SOURCE, actual.originalSource)
  }

  @Test
  def checkedParse1(): Unit = {
    val session: SparkSession = SparkSession.builder.master("local").getOrCreate()
    import session.sqlContext.implicits._

    val parsedDS = session.read.option("header", value = true).csv("src/test/resources/STD_sample.csv").as[STDRow]
    val actual: STDRowInfo = parsedDS.map(_.toMFRowInfo).head(3).last

    assertEquals("WWOR", actual.NET_SH_NETWORK)
    assertEquals("13:42", actual.TOT_NPG)
    assertEquals("10:13", actual.LOC_NPG)
    assertEquals("0", actual.NAT_PROPCT)
    val expectedText = "WWOR(IND),13:42,06.97,01:58,05.21,06.97,01:58,05.21,06.97,01:58,05.21,3:29,0:02,3:27,0:00,0:00,10:13,8:32,0:05,1:34,3:29,99,0"
    assertEquals(expectedText, actual.text)
    assertEquals(RowInfo.MAINFRAME_SOURCE, actual.originalSource)
  }

  @Test
  def timeChangeAddZero(): Unit = {
    val input = "1:11"

    val actual = STDRow.timeFix(input)

    assertEquals("01:11", actual)
  }

  @Test
  def timeChangeNull(): Unit = {
    val input = "null"

    val actual = STDRow.timeFix(input)

    assertEquals("00:00", actual)
  }

  @Test
  def timeChangeSimple(): Unit = {
    val input = "12:34"

    val actual = STDRow.timeFix(input)

    assertEquals("12:34", actual)
  }
}
